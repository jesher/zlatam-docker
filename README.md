# zlatam-docker

Criar rede interna para o Docker:
```
sudo docker network create rede_migration
```

Dentro da pasta "zlatam_mongo" execute:

```
sudo docker run -d -p 27017:27017 -v `pwd`/data:/data/db --network=rede_migration mongo  
```
Acessar a pasta "zlatam_migration", executar biuld:
```
sudo docker build -t zlatam_migration .
```
Ainda dentro da pasta, execute:

```
sudo docker run -it -v `pwd`/files:/code --network=rede_migration z_python_migration
```
