# Zlatam-api

### Comandos para iniciar ambiente da API:

Criar Docker Imagem:
```
sudo docker build -t zlatam_api .
```

Criar container:
```
sudo docker run -it -p 3001:3001 -v `pwd`/api:/code --network=rede_migration zlatam_api
```

Executar API no container:

```
npm run dev
```

### Comandos para iniciar um projeto node.js novo:

Dependencias:
```
npm install -D nodemon
npm install cors
npm install express
npm install mongoose
npm install mongoose-paginate
npm install require-di
npm install python-shell
```

executar dentro de import

```
export OPENBLAS_NUM_THREADS=1
```
