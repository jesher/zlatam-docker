const express = require('express');
const mongoose = require('mongoose');
const requireDir = require('require-dir');
const cors = require('cors');

// Iniciar o App
const app = express();
app.use(express.json());
app.use(cors());
//Inicia o db
mongoose.connect('mongodb://172.18.0.2:27017/model_tonton', {useNewUrlParser: true});
// Referencia Model criado
requireDir('./src/models');

// Rotas
app.use("/api", require("./src/routes"));

app.listen(3001);
