const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Recommends_customer = new mongoose.Schema({
    id:{
      type: Number,
      required: true,
    },
    customer_id:{
      type: Number,
      required: true,
    },
    customer_name:{
      type: String,
      required: true,
    },
    item_recommend:{
       type: [{
         "transaction_id": String,
         "description": String
       }],
       required: true,
     },

});

Recommends_customer.plugin(mongoosePaginate);

//mongoose.model('teste2', Recommends_customer);
mongoose.model('modelos_customer', Recommends_customer);
