const express = require('express');
const routes = express.Router();

const ProcessingController = require('./controllers/ProcessingController');
const RecommendController = require('./controllers/RecommendController');

//rotas
routes.get("/recommend/processing/:id", ProcessingController.processing);
routes.get("/recommend",                RecommendController.show);
routes.get("/recommend/:id",            RecommendController.findRecommend);
routes.get("/recommend/teste",          RecommendController.migrate);


module.exports = routes;
