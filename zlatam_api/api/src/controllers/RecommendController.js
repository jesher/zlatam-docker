const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const Recommend = mongoose.model('modelos_customer');

module.exports = {
  async migrate(req, res){
    Recommend.create({
      id:1,
      customer_id:1,
      customer_name:"teste",
      item_recommend: [{transaction_id : "teste",
			                   description : "teste"}]
    });
    return res.send("Teste com Sucesso!");
  },
  async show(req, res){
    const { page = 1} = req.query;
    const recommend = await Recommend.paginate({}, {page, limit: 10});
    return res.json(recommend);
  },
  async findRecommend(req, res){
    //const product = await Recommend.findById(req.params.id);
    const product = await Recommend.findOne({ customer_id: req.params.id }, function (err, adventure) {});
    return res.json(product);
  }
};
