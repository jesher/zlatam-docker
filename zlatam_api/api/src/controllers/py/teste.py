from pymongo import MongoClient

client = MongoClient('mongodb://172.18.0.2:27017/')
database = client["model_tonton"]
collection = database["clientes_teste"]


query = {
    "customer_id": 17581
}
sort = [("_id", -1)]

cursor = collection.find(query, sort=sort, limit=1000)
try:
    for doc in cursor:
        print(doc["name"])
finally:
    cursor.close()
