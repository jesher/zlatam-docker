const mongoose = require('mongoose');
const Product = mongoose.model('Product');
let {PythonShell} = require('python-shell')

module.exports = {

  async processing(req, res){
    console.log(req.params.id);
    PythonShell.run('Process_recommend.py',{ scriptPath: '/code/src/controllers/py', args: [req.params.id] }, function (err, results) {
  // script finished
    console.log(err);
    if (err) throw error;
    console.log('finished');

    res.send("ok");
    });

  }
};
