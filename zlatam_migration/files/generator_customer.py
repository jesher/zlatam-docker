import pandas as pd
import names
from dateutil.parser import parse
from pymongo import MongoClient

cliente = MongoClient('mongodb://172.18.0.2:27017/')
db = cliente['rcom']
clientes = db['clients_customer']

t = pd.read_csv('./script/csv/sales.csv', sep=';', names=['transaction_id',
                                                             'item_id',
                                                             'description',
                                                             'qty',
                                                             'invoice_date',
                                                             'unit_price',
                                                             'customer_id',
                                                             'location'], engine='python')

df = pd.DataFrame(t)

df.drop_duplicates('customer_id',inplace=True)
df = df.reset_index()
count = 2
for row in range(0, len(df)):
   clientes_j = {
                     "id": count,
                     "customer_name": str(names.get_full_name()),
                     "customer_id": float(df['customer_id'].loc[row])
                  }
   print(clientes_j)
   count += 1
   clientes.insert_one(clientes_j)
